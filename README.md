# Base Notify 

Provides a simple application for sending mass SMS messages using the Twilio API.

To use it, you need a twilio API account.

## Setup

Make the files available via a web server. No server side scripting (PHP, etc.) is needed.

You need to make the following dependencies available:

    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap-theme.min.css">
    <script src="node_modules/pouchdb/dist/js/pouchdb.min.js"></script>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

The easiest way to do that is via npm:

    npm install

## Data

All data is stored in your browser. It using [pouchdb](https://pouchdb.com/), so optionally you can synchronize your data to a server to share it with others.

## Demo

You can see a [live demo](https://sms.workingdirectory.net/).
