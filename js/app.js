(function() {

  'use strict';

  var ENTER_KEY = 13;
  var sentCount = 0;
  var localDb = 'basenotify';
  var twilio_api_url = "https://api.twilio.com/2010-04-01/Accounts/";
  var start_regexp = /start/i;
  var stop_regexp = /stop/i;

  // EDITING STARTS HERE (you dont need to edit anything above this line)

  var db = new PouchDB(localDb);
  var remoteDb = null; 

  db.changes({
    since: 'now',
    live: true
  }).on('change', showContacts);

  function updateSyncStatus() {
    showContacts();
  }

  // We have to create a new contact document and enter it in the database
  function addContact(input) {
    // Take the last part of the space-separate input, we expect that to
    // look like a phone number. The first part will be the name.
    var input = parseInput(input);
    if (!isPhoneNumber(input.mobile)) {
      alert("Please enter phone number, without spaces, as last part of your entry, e.g. Jose Sanchez +1.917.505.3434"); 
      return false;
    }
    var contact = {
      _id: new Date().toISOString(),
      name: input.name,
      mobile: input.mobile
    };
    db.put(contact).then(function(res) {
      tellUser('Contact added', 'success');
      $("#new-contact").val('');
    }).catch(function(err) {
      console.log(err);
    });
  }

  function isPhoneNumber(text) {
    var tel_regexp = /\+1([0-9]{10})/
    return tel_regexp.test(text);
  }

  function parseInput(text) {
    console.log("start: " + text);
    var ret = {};
    var pieces = text.trim().split(/[ ]+/);
    // Pop off the phone number from the end.
    var mobile = pieces.pop()
    ret.mobile = stripPhoneNumber(mobile);
    // Put the beginning back together to form the name.
    ret.name = pieces.join(' ');
    console.log(ret);
    return ret;
  }

  // Strip all non-numeric values from the passed in text
  function stripNumber(text) {
    return text.replace(/[^0-9]/g, '');
  }
  // Strip all non-numeric values from the passed in text and return with a leading +.
  function stripPhoneNumber(text) {
    return '+' + stripNumber(text);
  }

  // Show the current list of contacts by reading them from the database
  function showContacts() {
    db.allDocs({include_docs: true, descending: true}, function(err, doc) {
      redrawContactsUI(doc.rows);
    });
  }

  // User pressed the delete button for a contact, delete it
  function deleteButtonPressed(contact) {
    db.remove(contact).then(function(res) {
      tellUser("Contact was deleted.", 'success');
      return res;
    }).catch(function(err) {
      console.log(err);
    });
  }

  // Initialise a sync with the remote server
  function sync() {
    if(remoteDb) {
      //var opts = {live: true, retry: true, timeout:2000};
      var opts = {live: false, timeout: "2000"};
      console.log("setting sync");
      PouchDB.sync(localDb, remoteDb, opts).on('change', function(data) {
        // tellUser("Synchronized database with remote server.");
        console.log("Sync changed data with remote server.");
        console.log(data);
      }).on('pause', function (err) {
        // tellUser("Replication has paused.");
        console.log("Replication has paused (offline?).");
        console.log(err);
      }).on('active', function () {
        $("#last-sync-status").html("Successful sync on " + new Date().toISOString());
        console.log("Replication is active.");
      }).on('complete', function () {
        $("#last-sync-status").html("Successful sync on " + new Date().toISOString());
        console.log("Replication is complete.");
      }).on('denied', function(err) {
        tellUser("There was a permission error replicating");
        $("#last-sync-status").html("Failed to sync (permission error) on " + new Date().toISOString());
        console.log("denied");
        console.log(err);
      }).on('error', function(err) {
        tellUser("There was an error replicating");
        $("#last-sync-status").html("Failed to sync on " + new Date().toISOString());
        console.log(err);
      });
    }
  }

  // User has double clicked a contact, display an input so they can edit the record. 
  function contactDblClicked(contact) {
    var inputEditContact = document.createElement('input');
    inputEditContact.id = 'input_' + stripNumber(contact._id);
    inputEditContact.className = 'editing';
    inputEditContact.value = contact.name + " " + contact.mobile;
    inputEditContact.addEventListener('keypress', contactKeyPressed.bind(this, contact));

    var td = document.createElement('td');
    td.colSpan = "3";
    td.appendChild(inputEditContact);

    var tr = document.createElement('tr');
    tr.appendChild(td);

    $("#tr_" + stripNumber(contact._id)).replaceWith(tr);
    inputEditContact.focus();
  }

  // If they press enter while editing an entry, save
  function contactKeyPressed(contact, event) {
    if (event.keyCode === ENTER_KEY) {
      var trimmedText = event.target.value.trim();
      var input = parseInput(trimmedText);
      if (!isPhoneNumber(input.mobile)) {
        alert("Please enter phone number, without spaces, as last part of your entry, e.g. Jose Sanchez +1.917.505.3434"); 
        return;
      }
      contact.name = input.name;
      contact.mobile = input.mobile;
      db.put(contact).then(function(res) {
        tellUser("Contact was updated.", 'success');
        return res;
      }).catch(function(err) {
        console.log(err);
      });
    }
  }

  // Given an object representing a contact, this will create a list item
  // to display it.
  function createContactListItem(contact) {
    var tr = document.createElement('tr');
    tr.addEventListener('dblclick', contactDblClicked.bind(this, contact));
    if(contact.stopped) {
      tr.className = 'view stopped';
    }
    else {
      tr.className = 'view';
    }

    tr.id = 'tr_' + stripNumber(contact._id); 

    var td = document.createElement('td');
    td.appendChild( document.createTextNode(contact.name));
    tr.appendChild(td);

    td = document.createElement('td');
    td.appendChild( document.createTextNode(contact.mobile));
    tr.appendChild(td);

    return tr;
  }

  function redrawContactsUI(contacts) {
    var header = "<caption>Contacts (double click to edit)</caption><tr><th>Name</th><th>Mobile</th></tr>";
    $("#contact-list").html(header);

    contacts.forEach(function(contact) {
      // Don't return our config document.
      if(contact.doc._id == 'globalConfig') {
        displayConfig(contact.doc);
        return;
      }
      if(/^_design/.test(contact.doc._id)) {
        return;
      }
      // Add contact to the table.
      $("#contact-list").append(createContactListItem(contact.doc));
      // Make a delete button appear if we hover.
      var id = stripNumber(contact.doc._id);
      $("#tr_" + id).hover(
        function() {
          var link = '<button class="delete" id="delete_' + id + '">&#10006;</a>'; 
          $("#tr_" + id + " td").first().append(link);
          $("#delete_" + id).click(function() {
            deleteButtonPressed(contact.doc);
          });

        },
        function() {
          $("#delete_" + id).remove();
        }
      );
    });
  }

  function newContactKeyPressHandler( event ) {
    if (event.keyCode === ENTER_KEY) {
      addContact($("#new-contact").val());
    }
  }
  function newContactButtonClickHandler() {
    addContact($("#new-contact").val());
  }

  function newMessageKeyPressHandler( event ) {
    if (event.keyCode === ENTER_KEY) {
      sendMessageToAll();
    }
  }

  function newMessageButtonClickHandler() {
    sendMessageToAll();
  }

  function sendMessageToAll() {
    if($("#new-message").val() == "") {
      alert("Please type a message to send.");
      return;
    }
    if(!isTwilioConfigured()) {
    alert("Please sign up for a Twilio account and then fill in the Twilio configuration.");
      return;
    }

    var result = confirm("Are you sure you want to send this message: " + $("#new-message").val());
    if (result == true) {
      sentCount = 0;
      db.allDocs({include_docs: true, descending: true}, function(err, doc) {
        doc.rows.forEach(function(contact) {
          if(contact.doc._id != "globalConfig" && contact.doc.mobile &&!contact.doc.stopped) {
            sendMessage(contact.doc.mobile, $("#new-message").val());
          }
        });
      });
    }
    else {
      tellUser("Not sending");
    }
  }
  
  function isTwilioConfigured() {
    if ($("#accountSid").val() == "" || $("#authToken").val() == "" || $("#fromCell").val() == "") {
      return false;
    }
    else {
      return true;
    }
  }
  function sendMessage(to, msg) {
    var url = twilio_api_url  + $("#accountSid").val() + "/Messages.json";
    var data = {
      To: to,
	    From: $("#fromCell").val(),
      Body: msg,
    };
    var authHeader = 'Basic ' + btoa($("#accountSid").val() + ":" + $("#authToken").val());
    tellUser("Sending to " + to);
    $.ajax({
      type: "POST",
      url: url, 
      data: data,
      crossDomain: true,
      beforeSend: function (xhr) { 
        // NOTE: For some reason, using the jquery.ajax username 
        // and password fields don't work, but this dos.
        xhr.setRequestHeader('Authorization', authHeader);
      },
      success: function(jsondata) {
        sentCount++;
        tellUser("Sent " + sentCount + " message(s)", 'success');
      },
      timeout: 10000,
      error: function(request, textStatus, errorThrown) {
        console.log("Error sending via Twilio");
        tellUser("There was an error sending the message to " + to, 'warning');
        if (textStatus == 'timeout') {
          tellUser("The connection to Twilio timed out.", 'error');
        }
        console.log(errorThrown);
      },

    });
    return;
  }

  function tellUser(text, level=null) {
    console.log(text);
    if(level == "success" || level == "warning") {
      $("#status").removeClass("alert-info").addClass("alert-" + level);
    }
    updateStatus(text);
  }

  function dismissAlert() {
    $("#alert-dismiss").hide();
    $("#alert-message").html("&nbsp;");
    $("#status").removeClass("alert-warning").removeClass("alert-success").addClass("alert-info");
  }

  function updateStatus(text) {
    $("#alert-dismiss").show();
    $("#alert-message").html(text);
    // setTimeout ( clearStatus, 2000 );
  }

  function clearStatus() {
    $("#status").html('');
    $("#status").removeClass("alert-warning").removeClass("alert-success").addClass("alert-info");
  }

  function addEventListeners() {
    $("#new-contact").keypress(newContactKeyPressHandler);
    $("#new-message").keypress(newMessageKeyPressHandler);
    $("#accountSid").keypress(globalConfigKeyPressHandler);
    $("#authToken").keypress(globalConfigKeyPressHandler);
    $("#fromCell").keypress(globalConfigKeyPressHandler);
    $("#remoteDb").focusout(remoteDbUpdate);
    $("#alert-dismiss").click(dismissAlert);
    $("#retrieve-sms-messages").click(retrieveSmsMessages);
    $("#toggle-configuration").click(toggleConfiguration);
    $("#send-sms-message").click(newMessageButtonClickHandler);
    $("#add-contact").click(newContactButtonClickHandler);
    $("#sync-now").click(sync);
  }

  function toggleConfiguration() {
    if ($(this).html() == "Show Configuration") {
      $(this).html("Save Configuration");
    }
    else {
      $(this).html("Show Configuration");
      // Save remoteDb value.
      remoteDbUpdate();

      // Save global config values.
      db.get('globalConfig').catch(function(err) {
        if (err.name == "not_found") {
          // Return an empty doc
          var ret = getDefaultGlobalConfig(); 
          return ret;
        } 
        else {
          throw err;
        }
      }).then(function(globalConfig) {
        globalConfig['authToken'] = $("#authToken").val();
        globalConfig['fromCell'] = $("#fromCell").val();
        globalConfig['accountSid'] = $("#accountSid").val();
        return db.put(globalConfig);
      }).then(function(res) {
        tellUser("Updated configuration.", 'success');
        return res;
      }).catch(function(err) {
        tellUser("Failed to update configuration: " + err.message, 'warning');
        console.log(err);
      });
    }
  }

  function retrieveSmsMessages() {
    // Get globalConfig so we can see when we last retrieved messages
    // from Twilio.
    db.get('globalConfig').then(function(res) {
      var since = null;
      var alreadyProcessed = [];
      if(res.twilioIncomingLastChecked) {
        since = res.twilioIncomingLastChecked;
      }
      if(res.twilioIncomingAlreadyProcessed) {
        alreadyProcessed = res.twilioIncomingAlreadyProcessed;
      }
      // Now query Twilio.
      console.log("Retrieving SMS messages since " + since);
      var url = twilio_api_url  + $("#accountSid").val() + "/Messages.json";
      var data = {
        To: $("#fromCell").val(),
      };
      if(since) {
        data['DateSent>'] = since;
      }
      var authHeader = 'Basic ' + btoa($("#accountSid").val() + ":" + 
        $("#authToken").val());
      $.ajax({
        type: "GET",
        url: url, 
        data: data,
        beforeSend: function (xhr) { 
          // NOTE: For some reason, using the jquery.ajax username 
          // and password fields don't work, but this dos.
          xhr.setRequestHeader('Authorization', authHeader);
        },
        success: function(resp) {
          // Pass on the alreadyProcessed messages so we don't process them
          // twice.
          processTwilioIncomingMessageResponse(resp, alreadyProcessed);
        },
        timeout: 10000,
        error: function(request, textStatus, errorThrown) {
          console.log("Error retrieving via Twilio");
          tellUser("There was an error getting message.", 'warning');
          if (textStatus == 'timeout') {
            tellUser("The connection to Twilio timed out while retrieving..", 'error');
          }
          console.log(errorThrown);
        }
      }).catch(function(err) {
        console.log(err);
      });
    });
  }

  function processTwilioIncomingMessageResponse(resp, alreadyProcessed) {
    console.log("Retrieved messages from Twilio.");
    // Process *all* the messages received before we take the next step.
    var processSidsPromises = resp.messages.map(function(message) {
      return processMessage(message, alreadyProcessed);
    });
    Promise.all(processSidsPromises).then(function(processedSids) {
      // Now that we have processed all messages, updated our status.
      console.log("All promises complete.");
      console.log(processedSids);
      db.get('globalConfig').then(function(res) {
        var previouslyProcessedCount = res.twilioIncomingAlreadyProcessed.length;
        var totalProcessedCount = processedSids.length;
        var processedThisRun = totalProcessedCount - previouslyProcessedCount;
        // Update the alreadyProcessed variable to include all the
        // messages that are returned as of today.
        res.twilioIncomingAlreadyProcessed = processedSids;
        // Update the date last checked to be today. Future checks will always be
        // from this date forward.
        // Get date in YYYY-mm-dd format. Dates are always GMT (as on twilio).
        res.twilioIncomingLastChecked = new Date().toISOString().split('T')[0];
        // Update globalConfig with new values.
        tellUser("I checked for new Twilio messages. Found " + processedThisRun + " message(s)");
        return db.put(res);
      }).then(function(res) {
        console.log("Updated Config with new Twilio Received data.");
      }).catch(function(err) {
        console.log(err);
      });
    }, function(rejectedSids) {
      console.log("Rejected Sids");
      console.log(rejectedSids);
      tellUser("I ran into a problem pulling in messages.");
    }).catch(function(err) {
      console.log(err);
    });
  }

  function processMessage(message, alreadyProcessed) {
    return new Promise(function(resolve, reject) {
      var Sid = message.sid;
      var From = message.from;
      var Body = message.body;
      var Status = message.status;
      console.log(Sid + " " + From + " " + Body + " " + Status);
      if(-1 !== $.inArray(Sid, alreadyProcessed)) {
        console.log("Already processed: " + Sid);
        resolve(Sid);
      }
      else {
        db.find({
          selector: {mobile: From},
          fields: ['_id', 'stopped']
        }).then(function (results) {
          // If there is no match in our database for this number. 
          if(!results.docs.length) {
            if (start_regexp.test(Body)) {
              console.log("Starting new contact: " + From);
              var data = {
                _id: new Date().toISOString(),
                name: "",
                mobile: From
              }
              db.put(data).then(function(res) {
                console.log("New contact started: " + Sid);
                resolve(Sid);
              }).catch(function(err) {
                console.log("Error starting new contact: " + Sid);
                console.log(err);
                tellUser("I ran into an error starting " + From);
                reject(Sid);
              });
            }
            else if (stop_regexp.test(Body)) {
              console.log("Contact " + From + " wants to stop, but not in database: " + Sid);
              resolve(Sid);
            }
            else {
              console.log("Ignoring message " + From + " (not in our database) with message " + Body + " And Sid: " + Sid);
              resolve(Sid);
            }
          }
          else {
            // There is a match in our database. There might be more then one. 
            // Iterate over all of them.
            results.docs.forEach(function(result) {
              var get = result._id;
              if (start_regexp.test(Body)) {
                console.log("Found existing record, wants to start " + From);
                if(result.stopped) {
                  db.get(get).then(function(data) {
                    data["stopped"] = null;
                    return db.put(data);
                  }).then(function(res) {
                    console.log("Contact started: " + Sid);
                    resolve(Sid);
                  }).catch(function(err) {
                    console.log("Error starting contact: " + Sid);
                    reject(Sid);
                    tellUser("I ran into an error starting " + From);
                    console.log(err);
                  });
                }
                else {
                  console.log("But they are already started: " + Sid);
                  resolve(Sid);
                }
              }
              else if (stop_regexp.test(Body)) {
                console.log("Found existing record, wants to stop " + From);
                if(!result.stopped) {
                  // Update contact, stop them.
                  db.get(get).then(function(data) {
                    data["stopped"] = new Date().toISOString();
                    return db.put(data);
                  }).then(function(res) {
                    console.log("Contact stopped: " + Sid);
                    resolve(Sid);
                  }).catch(function(err) {
                    console.log("Error stopping contact: " + Sid);
                    console.log(err);
                    tellUser("I ran into an error stopping " + From);
                    reject(Sid);
                  });
                }
                else {
                  console.log("But they are already stopped: " + Sid);
                  resolve(Sid);
                }
              }
              else {
                console.log("Ignoring message " + From + " with message " + Body + " And Sid: " + Sid);
                resolve(Sid);
              }
            });
          }
        }).catch(function(err) {
          console.log("Error with: " + Sid);  
          console.log(err);
          tellUser("I ran into an error trying to locate " + From);
          reject(Sid);
        });
      }
    });
  }

  function getDefaultGlobalConfig() {
    return {
      _id: 'globalConfig',
      accountSid: null,
      accountToken: null,
      fromCell: null
    };
  }

  function globalConfigKeyPressHandler(event) {
    // Only save when enter is pushed
    if (event.keyCode === ENTER_KEY) {
      var key = event.target.id;
      var value = event.target.value.trim();
      db.get('globalConfig').catch(function(err) {
        if (err.name == "not_found") {
          // Return an empty doc
          var ret = getDefaultGlobalConfig(); 
          return ret;
        } 
        else {
          throw err;
        }
      }).then(function(globalConfig) {
        globalConfig[key] = value;
        return db.put(globalConfig);
      }).then(function(res) {
        tellUser("Updated configuration.", 'success');
        // Blur to give some user feedback that is has been saved.
        $("#" + key).blur();
        return res;
      }).catch(function(err) {
        tellUser("Failed to update configuration: " + err.message, 'warning');
        console.log(err);
      })
    }
  }

  function remoteDbUpdate(event) {
    var remoteDbString = $("#remoteDb").val();
    // Save to localStorage (in case they are not using a password manager).
    localStorage.setItem('remoteDb', remoteDbString); 
    if(remoteDbString) {
      // Set the remoteDb global variable
      remoteDb = new PouchDB(remoteDbString);
      if(remoteDb) {
        console.log("Remote DB is set and ready to rock.");
        // Re-sync, since the remote db may have changed
        sync();
      }
      else {
        console.log("Remote DB was not set successfully");
      }
    }
    updateRemoteDbDisplay();
  }

  function displayConfigValues(globalConfig) {
    $("#accountSid").val(globalConfig.accountSid);
    $("#authToken").val(globalConfig.authToken);
    $("#fromCell").val(globalConfig.fromCell);
  }

  function displayConfig(globalConfig = null) {
    if(globalConfig) {
      displayConfigValues(globalConfig);
    }
    else {
      db.get('globalConfig').catch(function(err) {
        if (err.name == "not_found") {
          // Return an empty doc
          var def = getDefaultGlobalConfig(); 
          return def;
        } 
        else {
          throw err;
        }
      }).then(function(globalConfig) {
        displayConfigValues(globalConfig); 
      }).catch(function(err) {
        console.log(err);
      });
    }
    setRemoteDbFromLocalStorage(); 
  }

  function setRemoteDbFromLocalStorage() {
    // Pull in the RemoteDb. If it is already filled in (e.g. via
    // a password manager), then don't overwrite. Otherwise, pull in from
    // localStorage.
    if(!$("#remoteDb").val()) {
      $("#remoteDb").val(localStorage.getItem("remoteDb")); 
    }
  }

  function updateRemoteDbDisplay() {
    // If we have a value, extract the non-sensitive parts so we can give 
    // some user feedback.
    if($("#remoteDb").val()) {
      var a = document.createElement('a');
      a.href = $("#remoteDb").val();
      var string = '';
      if (a.username) {
        string += a.username;
      }
      if (a.hostname) {
        string += "@" + a.hostname;
      }
      if (a.pathname) {
        string += a.pathname;
      }
      if (string) {
        string = "Sync'ing with: " + string;
        $("#remote-database-details").html(string);
      }
    }
  }

  function createIndices() {
    db.createIndex({
      index: {
        fields: ['mobile']
      }
    }).then(function (res) {
      if(res.result == 'exists') {
        console.log("Indices already exist.");
      }
      else {
        console.log("Created Indices.");
      }
    }).catch(function (err) {
      console.log("Error creating indices.");
      console.log(err);
    });
  }
  addEventListeners();
  createIndices();
  showContacts();
  setRemoteDbFromLocalStorage(); 
  remoteDbUpdate();
  sync();
  

})();
